<html>
    <head>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <style>
            body {
                padding: 60px 0px;
            }
            .navbar-collapse {
                position: relative;
                padding-top: 30px !important;
                max-height: 270px;
            }
            .navbar-collapse form[role="search"] {
                position: absolute;
                top: 0px;
                right: 0px;
                width: 100%;
                padding: 0px;
                margin: 0px;
                z-index: 0;
            }
            .navbar-collapse form[role="search"] button,
            .navbar-collapse form[role="search"] input {
                padding: 8px 12px;
                border-radius: 0px;
                border-width: 0px;
                color: rgb(119, 119, 119);
                background-color: rgb(248, 248, 248);
                border-color: rgb(231, 231, 231);
                box-shadow: none;
                outline: none;
            }
            .navbar-collapse form[role="search"] input {
                padding: 16px 12px;
                font-size: 14pt;
                font-style: italic;
                color: rgb(160, 160, 160);
                box-shadow: none;
            }
            .navbar-collapse form[role="search"] button[type="reset"] {
                display: none;
            }

            @media (min-width: 768px) {
                .navbar-collapse {
                    padding-top: 0px !important;
                    padding-right: 38px !important;
                }
                .navbar-collapse form[role="search"] {
                    width: 38px;
                }
                .navbar-collapse form[role="search"] button,
                .navbar-collapse form[role="search"] input {
                    padding: 15px 12px;
                }
                .navbar-collapse form[role="search"] input {
                    font-size: 18pt;
                    opacity: 0;
                    display: none;
                    height: 50px;
                }
                .navbar-collapse form[role="search"].active {
                    width: 100%;
                }
                .navbar-collapse form[role="search"].active button,
                .navbar-collapse form[role="search"].active input {
                    display: table-cell;
                    opacity: 1;
                }
                .navbar-collapse form[role="search"].active input {
                    width: 100%;
                }
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Test Site</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php var_dump(Auth::check()); ?>
                    @if(!Auth::check())
                        <ul class="nav navbar-nav navbar-right">
                            <li class="{{ Request::segment(1) == 'login' ? 'active' : '' }}"><a href="/login">Login</a></li>
                            <li class="{{ Request::segment(1) == 'register' ? 'active' : '' }}"><a href="/register">Register</a></li>
                        </ul>
                     @else
                        logged
                    @endif
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        @yield('content')

        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script>
            function login(params) {
                var user = $(params).closest('form').find('input[name="email"]').val(),
                    password = $(params).closest('form').find('input[name="password"]').val(),
                    url = $(params).closest('form').data('url');

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: $(params).closest('form').serialize(),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', make_base_auth(user, password));
                    },
                    success: function (response) {
                        console.log(response);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        error_messages(jqXHR.responseJSON.errors)
                    }

                });

                function error_messages(errors) {
                    $('.alert').remove();

                    $.each( errors, function( index, value ){
                        var message =  value.join("<br/>");
                        $("[name='"+index+"']").after('<div style="margin-top: 10px;" class="alert alert-danger"><strong>'+message+'</div></div>');
                    });
                }
                function make_base_auth(user, password) {
                    var tok = user + ':' + password;
                    var hash = btoa(tok);
                    return 'Basic ' + hash;
                }
            }
        </script>
    </body>
</html>