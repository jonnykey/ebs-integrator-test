@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row">
            <form data-url="/api/register">
                <div class="form-group">
                    <label for="email">Your name:</label>
                    <input type="name" name="name" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" name="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" name="password" class="form-control" id="pwd">
                </div>
                <div class="form-group">
                    <label for="pwd">Repeat password:</label>
                    <input type="password" name="repeat_password" class="form-control" id="pwd_repeat">
                </div>
                <button onclick="login(this); return false;" class="btn btn-default">Register</button>
            </form>
        </div>
    </div>
@endsection
