@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row">
            <form data-url="/oauth/authorize">
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" name="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" name="password" class="form-control" id="pwd">
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label> <input name="remember_me" type="checkbox"> Save password </label>
                    </div> <!-- checkbox .// -->
                </div> <!-- form-group// -->
                <button onclick="login(this); return false;" class="btn btn-default">Login</button>
            </form>
        </div>
    </div>
@stop