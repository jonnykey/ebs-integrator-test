<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            return auth()->user()->can($request->route()->getName())
                ? $next($request)
                : response()->json(['errors'=>'Your don\'t have permission to this page!'], 403);
    }
}
