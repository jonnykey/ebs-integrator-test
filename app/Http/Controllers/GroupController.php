<?php

namespace App\Http\Controllers;

use App\Group;
use App\Permission;
use Illuminate\Http\Request;
use Validator;

class GroupController extends Controller
{
    public function list()
    {
        $list = Group::get();

        return response()->json(['success' => $list], 200);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $slug = str_slug($input['name']);

        $group = new Group();
        if($group->checkIsCreated($slug))
            return response()->json(['errors'=>'This group with current name is created. Please select another name!'], 401);

        $input['slug'] = $slug;
        $result = $group->create($input);

        return response()->json(['success' => $result], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:groups',
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 403);
        }

        $input = $request->all();

        $group = Group()::find($input['id']);
        $group->update($input);

        return response()->json(['success' => $group], 200);
    }

    public function delete(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:groups',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 403);
        }
        $input = $request->all();

        $group = Group()::find($input['id']);
        $group->delete();

        return response()->json(['success' => 'Group removed successfully with child permissions!'], 200);
    }

    public function addPermission(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|integer|exists:groups,id',
            'permission_id' => 'required|integer|exists:permissions,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 403);
        }

        $input = $request->all();

        $group = Group::find($input['group_id']);

        if (! $group->permissions->contains($input['permission_id'])) {
            $group->permissions()->attach($input['permission_id']);
        }

        return response()->json(['success' => 'Permission added successfully to group!'], 200);
    }

    public function removePermission(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|integer|exists:groups,id',
            'permission_id' => 'required|integer|exists:permissions,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 403);
        }

        $input = $request->all();

        $group = Group::find($input['group_id']);

        if ($group->permissions->contains($input['permission_id'])) {
            $group->permissions()->detach($input['permission_id']);
        }

        return response()->json(['success' => 'Permission detached successfully from group!'], 200);
    }
}
