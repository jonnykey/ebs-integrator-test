<?php

namespace App\Http\Controllers;

use App\Group;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    /**
     * @SWG\Post(
     *      path="/login",
     *      operationId="authentification",
     *      tags={"Authentification"},
     *      summary="Authentification",
     *      description="Authentification is system",
     *     @SWG\Parameter(
     *          name="authentification",
     *          description="Authentification",
     *          required=true,
     *          type="string",
     *          in="body",
     * 			@SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="email", type="string", example="ivan4ever92@gmail.com"),
     *              @SWG\Property(property="password", type="string", example="qazwsx"),
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * Login trough API
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();

            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;

            if (request('remember_me'))
                $token->expires_at = Carbon::now()->addWeeks(1);

            $token->save();

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ]);
        }
        else {
            $messages = [
                'confirmed' => 'Credentials are not correct!'
            ];
            $rules = [
                'email'           => 'required|max:255|email',
                'password'           => 'required|confirmed',
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);

            return response()->json(['errors'=>$validator->errors()],401);
        }
    }

    /**
     * @SWG\Post(
     *      path="/register",
     *      operationId="register",
     *      tags={"Authentification"},
     *      summary="Register",
     *      description="Registration is system",
     *     @SWG\Parameter(
     *          name="register",
     *          description="Register",
     *          required=true,
     *          type="string",
     *          in="body",
     * 			@SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="test"),
     *              @SWG\Property(property="email", type="string", example="user@mail.com"),
     *              @SWG\Property(property="password", type="string", example="password"),
     *              @SWG\Property(property="password_confirmation", type="string", example="password")
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * Register trough API
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'password' => 'required|min:6',
            'repeat_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('application')-> accessToken;
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], 200);
    }

    /**
     * @SWG\Post(
     *      path="/details",
     *      operationId="getDetailsUser",
     *      tags={"Details User"},
     *      summary="Details",
     *      description="Get details user",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"default": {}}
     *       }
     *     )
     *
     * Get details API
     */
    public function details()
    {
        $user = Auth::user();

        return response()->json(['success' => $user], 200);
    }


    public function list()
    {
        $user = User::all();

        return response()->json(['success' => $user], 200);
    }

    public function logout (Request $request) {

        $token = $request->user()->token();
        $token->revoke();

        $response = 'You have been succesfully logged out!';
        return response($response, 200);

    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'password' => 'required|min:6',
            'repeat_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('application')-> accessToken;
        $success['name'] =  $user->name;
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'max:255',
            'email' => 'max:255|email',
            'password' => 'min:6',
            'repeat_password' => 'same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $user = new User();
        $user = $user->find($input['id']);
        if(!$user)
            return response()->json(['errors'=>'User not found'], 401);

        if(isset($input['password']))
            $input['password'] = bcrypt($input['password']);

        $user->update($input);

        return response()->json(['success' => $user], 200);
    }

    public function delete(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $user = new User();
        $user = $user->find($input['id']);
        if(!$user)
            return response()->json(['errors'=>'User not found'], 401);

        $user->delete();

        return response()->json(['success' => 'User removed successfully!'], 200);
    }

    public function addToGroup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer|exists:users,id',
            'group_id' => 'required|integer|exists:groups,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 403);
        }

        $input = $request->all();

        $user = User::find($input['user_id']);

        if (! $user->groups->contains($input['group_id'])) {
            $user->groups()->attach($input['group_id']);
        }

        return response()->json(['success' => 'User added successfully to group!'], 200);
    }

    public function removeFromGroup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer|exists:users,id',
            'group_id' => 'required|integer|exists:groups,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 403);
        }

        $input = $request->all();

        $user = User::find($input['user_id']);

        if ($user->groups->contains($input['group_id'])) {
            $user->groups()->detach($input['group_id']);
        }

        return response()->json(['success' => 'User removed successfully from group!'], 200);
    }

    public function addPermission(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer|exists:users,id',
            'permission_id' => 'required|integer|exists:permissions,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 403);
        }

        $input = $request->all();

        $user = User::find($input['user_id']);

        if (! $user->permissions->contains($input['permission_id'])) {
            $user->permissions()->attach($input['permission_id']);
        }

        return response()->json(['success' => 'Permission is attached successfully to user!'], 200);
    }

    public function removePermission(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer|exists:users,id',
            'permission_id' => 'required|integer|exists:permissions,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 403);
        }

        $input = $request->all();

        $user = User::find($input['user_id']);

        if ($user->permissions->contains($input['permission_id'])) {
            $user->permissions()->detach($input['permission_id']);
        }

        return response()->json(['success' => 'Permission is detached successfully from user!'], 200);
    }

}