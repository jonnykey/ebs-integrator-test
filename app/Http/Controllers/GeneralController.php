<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }

    public function main(Request $request){
//        dd($request->bearerToken());
//        dd(Auth::guard('api')->id());
        $client = new Client();
        $res = $client->post(url('/api/accounts/list'), [
            'headers' => [
                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjhmZjhiMzE3ZDBiZWRkZmE5M2FhZjA5OGE3ZDhkNDI1NWYxZDk0MjFlNTk2ZjYxOTA2MGQ3NGZhNGI2MzQ1ZDUzOWQwZjhjYjk5YzMxZmE4In0.eyJhdWQiOiIxIiwianRpIjoiOGZmOGIzMTdkMGJlZGRmYTkzYWFmMDk4YTdkOGQ0MjU1ZjFkOTQyMWU1OTZmNjE5MDYwZDc0ZmE0YjYzNDVkNTM5ZDBmOGNiOTljMzFmYTgiLCJpYXQiOjE1NTQ3NjI0MzQsIm5iZiI6MTU1NDc2MjQzNCwiZXhwIjoxNTg2Mzg0ODM0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.Q6mvutRFJNV5gGAfYe0mCPbjA6n4uGF-LB5YkYi_t3AUvvY-voXOVDA2xMpfwQJGIYY79B9kDG_eDu844ly3kH0GSp6iLohuceY_ErmNTQqwaK3_CPdGDFl4feI6p2OeYcvNP-xcj8p3l6DiK0fgGCkrAEoAl2zLSOXADzVQmYlswje7eeodDcGXEGLsvaefmFPLEKSLQY9fiPQ78U0hxbkw4_1ebZL6iEKngo7f-2YN9sqZ9B1eXHHCm8eXYPHcPE4-iAnLPSj_I_e80FCvdoeWBvv1Qz6k-Z5V0ZdJ_JcW4pa_WsB5RwqsU8X8DRC9prIwwfHS5mAr36ezROuKei8ruSnctP5-PunKGP4QOkeGoQaJKaK8ZawAG5GvQyCYzi3lpbpTIc3MAJBcciwJu0nBanyHqbwdO5gJ3L8eqSssk-dtrmZaLcAWNpT8Z2yUD5S7ZFgEGH_GJMDk67PId2mid6QTFXg7-MdePftCM1kW0D0_mzY0Xryv-uqV8c_xztoTpvHtEP9y8ry2Urt0IQN0tK9a4Ckn6smj6YiPwuwIffa8LQpYhFhsGOXuoYgKtXHNplJaGTYcXPL47H_9SUmOiBmtBrsChB6oqECss7b_j-FQOQRrQrff_LMopqgpY1IDz9ezvczzfEKoXmNfLRYQxXSExC23T9bSEugNf30',
            ]
        ]);
        //echo $res->getStatusCode(); // 200
        echo $res->getBody();
        dd(json_decode($res->getBody())->success);
        return view('main');
    }
}
