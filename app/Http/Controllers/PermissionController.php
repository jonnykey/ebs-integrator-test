<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function list()
    {
        $list = Permission::get();

        return response()->json(['success' => $list], 200);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $slug = str_slug($input['name']);

        $permission = new Permission();
        if($permission->checkIsCreated($slug))
            return response()->json(['errors'=>'This permission with current name is created. Please select another name!'], 401);

        $input['slug'] = $slug;
        $result = $permission->create($input);

        return response()->json(['success' => $result], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:permissions,id',
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 403);
        }

        $input = $request->all();

        $permission = Permission::find($input['id']);
        $permission->update($input);

        return response()->json(['success' => $permission], 200);
    }

    public function delete(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:permissions,id',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 403);
        }
        $input = $request->all();

        $permission = Permission::find($input['id']);
        $permission->delete();

        return response()->json(['success' => 'Permission removed successfully with child permissions!'], 200);
    }

}
