<?php

namespace App;

use App\Permissions\HasGroupTrait;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasGroupTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug'
    ];

    public function permissions() {
        return $this->belongsToMany(Permission::class,'groups_permissions');
    }
}
