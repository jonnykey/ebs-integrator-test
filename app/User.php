<?php
/**
 * @SWG\Definition(
 *      required={"name","email"},
 *      type="object",
 *      @SWG\Xml(name="User")
 * ),
 * @SWG\Property(format="byte", type="string", property="name", description="User Name"),
 * @SWG\Property(format="byte", type="string",property="email", description="Email")
 * @SWG\Property(format="byte", type="string",property="password", description="Password")
 */

namespace App;

use App\Permissions\HasPermissionsTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasPermissionsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isActive()
    {
        return (bool) $this->is_active;
    }
}
