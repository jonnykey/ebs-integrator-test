<?php


namespace App\Permissions;


trait HasGroupTrait
{
    public function checkIsCreated($slug)
    {
        $result = (bool) $this->where('slug', $slug)->get()->count();
        if($result)
            return true;

        return false;
    }
}