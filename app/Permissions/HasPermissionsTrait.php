<?php
namespace App\Permissions;

use App\Group;
use App\Permission;

trait HasPermissionsTrait
{
    public function groups() {
        return $this->belongsToMany(Group::class,'users_groups');

    }

    public function permissions() {
        return $this->belongsToMany(Permission::class,'users_permissions');

    }

    public function hasGroup( ... $groups ) {
        foreach ($groups as $group) {
            if ($this->groups->contains('slug', $group)) {
                return true;
            }
        }
        return false;
    }

    public function hasPermissionTo($permission) {
        return $this->hasPermissionThroughGroup($permission) || $this->hasPermission($permission);
    }

    protected function hasPermission($permission) {
        return (bool) $this->permissions->where('slug', $permission->slug)->count();
    }

    public function hasPermissionThroughGroup($permission) {
        foreach ($permission->groups as $group){
            if($this->groups->contains($group)) {
                return true;
            }
        }
        return false;
    }

    public function givePermissionsTo(... $permissions) {
        $permissions = $this->getAllPermissions($permissions);

        if($permissions === null) {
            return $this;
        }
        $this->permissions()->saveMany($permissions);
        return $this;
    }

    public function deletePermissions( ... $permissions ) {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);
        return $this;
    }

    protected function getAllPermissions(array $permissions)
    {
        return Permission::whereIn('slug', $permissions)->get();
    }
}