<?php
//interface Additionable {
//    public function add($x, $y);
//}
//function average($a, $b) {
//    $anon = new class implements Additionable {
//        public function divide($x, $y) {
//            return $x / $y;
//        }
//        public function add($x, $y) {
//            return $x + $y;
//        }
//    };
//
//    $sum = $anon->add($a, $b);
//    $average = $anon->divide($sum, 2);
//
//    return $average;
//}
//echo average(10, 70);

//function getName(): ?string {
//    return "ElNi\u{{{00F1}}}o";
//}
//echo getName();

//function getReduced(int $x) {
//    $x--;
//    return $x;
//}
//function getIncreased(int $x): void {
//    $x++;
//    return $x;
//}
//$x = 0;
//$x = getReduced($x);
//$x = getIncreased($x);
//echo $x;

class MyCollection {
    private $coll = [];

    public function add(?int $x): void {
        $this->coll[] = $x ?? 0;
    }

    public function getElements(): iterable {
        return $this->coll;
    }
}
$collection = new MyCollection();
$collection->add(null);
$collection->add(1);
$collection->add(0);
print_r($collection->getElements());
