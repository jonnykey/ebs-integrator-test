<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');

Route::group(['middleware' => ['swfix','auth:api','check.user.active','check.user.permission']], function(){
    Route::group(['prefix'=>'accounts','as'=>'account.'], function() {
        Route::post('details', 'UserController@details')->name('details');
        Route::post('list', 'UserController@list')->name('list');
        Route::post('update', 'UserController@update')->name('update');
        Route::post('delete', 'UserController@delete')->name('delete');
        Route::post('add-to-group', 'UserController@addToGroup')->name('add_to_group');
    });
    Route::group(['prefix'=>'group','as'=>'group.'], function() {
        Route::post('list', 'GroupController@list')->name('list');
        Route::post('create', 'GroupController@create')->name('create');
        Route::post('update', 'GroupController@update')->name('update');
        Route::post('delete', 'GroupController@delete')->name('delete');
        Route::post('add-permission', 'GroupController@addPermission')->name('add-permission');
        Route::post('remove-permission', 'GroupController@removePermission')->name('remove-permission');
    });
    Route::group(['prefix'=>'permission','as'=>'permission.'], function() {
        Route::post('list', 'GroupController@list')->name('list');
        Route::post('create', 'GroupController@create')->name('create');
        Route::post('update', 'GroupController@update')->name('update');
        Route::post('delete', 'GroupController@delete')->name('delete');
    });
    Route::get('home', 'UserController@home');
});