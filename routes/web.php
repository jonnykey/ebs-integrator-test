<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login', array('uses' => 'LoginController@loginView'));
Route::get('/register', array('uses' => 'LoginController@registerView'));
Route::get('/main', array('as' => 'main', 'uses' => 'GeneralController@main'));
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
