<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routeCollection = app('router')->getRoutes();

        foreach ($routeCollection as $value) {
            $permission = new \App\Permission();
            $permission->slug = $value->getName();
            $permission->name = $value->getName();
            $permission->save();
        }
    }
}
