<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = \App\Permission::all();

        $role = new \App\Group();
        $role->slug = 'admin';
        $role->name = 'Admin';
        $role->save();

        foreach ($permissions as $permission) {
            $role->permissions()->attach($permission);
        }

    }
}
