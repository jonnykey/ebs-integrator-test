<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_group = \App\Group::where('slug','admin')->first();

        $user = new \App\User();
        $user->name = 'Admin';
        $user->email = 'admin@mail.com';
        $user->password = bcrypt('qazwsx');
        $user->save();
        $user->groups()->attach($admin_group);
    }
}
